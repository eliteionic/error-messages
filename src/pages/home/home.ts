import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { UsernameValidator } from '../../validators/username';
import { EmailValidator } from '../../validators/email';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	registerForm: FormGroup;

	passwordErrors: Object;
	usernameErrors: Object;
	emailErrors: Object;

	constructor(public navCtrl: NavController, public usernameValidator: UsernameValidator, public emailValidator: EmailValidator, public formBuilder: FormBuilder) {

	    this.registerForm = this.formBuilder.group({
	        username: ['', Validators.compose([Validators.maxLength(16), Validators.pattern('[a-zA-Z0-9]*'), Validators.required]), usernameValidator.checkUsername.bind(usernameValidator)],
	        email: ['', Validators.compose([Validators.maxLength(50), Validators.required]), emailValidator.checkEmail.bind(emailValidator)],
			password: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(30), Validators.required])]
	    });

		this.usernameErrors = {
			'usernameInUse': 'Sorry, this username is already taken',
		};

		this.emailErrors = {
			'emailInUse': 'Sorry, this email address is already in use',
		};

		this.passwordErrors = {
			'minlength': 'This password is not secure enough',
			'maxlength': 'Is there such a thing as too secure? Your password is a little long...'
		};

	}

}
