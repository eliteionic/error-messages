import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class EmailValidator {
 
  debouncer: any;

  constructor(){
    
  }

  checkEmail(control: FormControl): any {

    clearTimeout(this.debouncer);

    return new Promise(resolve => {
 
      this.debouncer = setTimeout(() => {

        resolve({'emailInUse': true})

      }, 2000);      
 
    });
  }

}
 