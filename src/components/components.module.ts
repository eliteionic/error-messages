import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ErrorMessagesComponent } from './error-messages/error-messages';

@NgModule({
	declarations: [ErrorMessagesComponent],
	imports: [IonicModule],
	exports: [ErrorMessagesComponent]
})
export class ComponentsModule {}
